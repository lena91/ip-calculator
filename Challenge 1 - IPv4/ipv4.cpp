#include "ipv4.h"

std::array<uint8_t, 4> computeNetwork(const std::array<uint8_t,4>& ipAddress, const std::array<uint8_t,4>& subnetMask)
{
	std::array<uint8_t, 4> networkAddress;
	
	for (int i = 0; i < 4; i++)
	{
		networkAddress[i] = ipAddress[i] & subnetMask[i];
	}

	return networkAddress;
}

std::array<uint8_t, 4> computeBroadcast(const std::array<uint8_t, 4>& subnetMask, const std::array<uint8_t, 4>& networkAddress)
{
	std::array<uint8_t, 4> broadcastAddress;
	
	for (int i = 0; i < 4; i++)
	{
		uint8_t dupa = ~subnetMask[i];
		broadcastAddress[i] = ~subnetMask[i] | networkAddress[i];
	}

	return broadcastAddress;
}

std::array<uint8_t, 4> computefirstHostAddress(const std::array<uint8_t, 4>& networkAddress, const std::array<uint8_t, 4>& subnetMask)
{
	std::array<uint8_t, 4> firstHostAddress = networkAddress;

	if (subnetMask[3] < 254)
		firstHostAddress[3] += 1;

	return firstHostAddress;
}

std::array<uint8_t, 4> computelastHostAddress(const std::array<uint8_t, 4>& broadcastAddress, const std::array<uint8_t, 4>& subnetMask)
{
	std::array<uint8_t, 4> lastHostAddress = broadcastAddress;

	if (subnetMask[3] < 254)
		lastHostAddress[3] -= 1;

	return lastHostAddress;
}

bool isValidIpAddress(std::string ipAddress)
{
	std::regex ipRegex("^(([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\\.){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])$", std::regex::extended);
	if (std::regex_match(ipAddress, ipRegex))
		return true;
	else
		return false;
}

bool isValidSubnetMask(std::string subnetMask)
{
	std::regex ipRegex("^(((255\\.){3}(255|254|252|248|240|224|192|128|0+))|((255\\.){2}(255|254|252|248|240|224|192|128|0+)\\.0)|((255\\.)(255|254|252|248|240|224|192|128|0+)(\\.0+){2})|((255|254|252|248|240|224|192|128|0+)(\\.0+){3}))$");
	if (std::regex_match(subnetMask, ipRegex))
		return true;
	else
		return false;
}

std::array<uint8_t, 4> readIpAddress()
{
	std::string text;
	std::cout << "Enter IP address: ";
	std::cin >> text;

	while (!isValidIpAddress(text))
	{
		std::cout << "Invalid IP address! Enter valid IP address: ";
		std::cin >> text;
	}

	return fillUpAddressArray(text);
}

std::array<uint8_t, 4> readSubnetMask()
{
	std::string text;
	std::cout << "Enter subnet mask: ";
	std::cin >> text;

	while (!isValidSubnetMask(text))
	{
		std::cout << "Invalid subnet mask! Enter valid subnet mask: ";
		std::cin >> text;
	}

	return fillUpAddressArray(text);
}

std::array<uint8_t, 4> fillUpAddressArray(std::string text)
{
	std::istringstream iss;
	iss.str(text);
	std::string token;
	std::array<uint8_t, 4> address;

	for (int i = 0; i < 4; i++)
	{
		std::getline(iss, token, '.');
		if (!token.empty())
			address[i] = uint8_t(std::stoi(token));
	}

	return address;
}

void computeAddresses()
{
	std::array<uint8_t, 4> ipAddress = readIpAddress();
	std::array<uint8_t, 4> subnetMask = readSubnetMask();
	std::array<uint8_t, 4> networkAddress = computeNetwork(ipAddress, subnetMask);
	std::array<uint8_t, 4> broadcastAddress = computeBroadcast(subnetMask, networkAddress);
	std::array<uint8_t, 4> firstHostAddress = computefirstHostAddress(networkAddress, subnetMask);
	std::array<uint8_t, 4> lastHostAddress = computelastHostAddress(broadcastAddress, subnetMask);

	printAllAddresses(ipAddress, subnetMask, networkAddress, broadcastAddress, firstHostAddress, lastHostAddress);
}

void printAddress(const std::array<uint8_t, 4>& address)
{
	for (int i = 0; i < 4; i++)
	{
		std::cout << static_cast<int>(address[i]);
		if (i < 3)
			std::cout << ".";
		else
			std::cout << std::endl;
	}
}

void printAllAddresses(const std::array<uint8_t, 4>& ipAddress, const std::array<uint8_t, 4>& subnetMask,
					   const std::array<uint8_t, 4>& networkAddress, const std::array<uint8_t, 4>& broadcastAddress,
					   const std::array<uint8_t, 4>& firstHostAddress, const std::array<uint8_t, 4>& lastHostAddress)
{
	std::cout << std::endl << "IP ADDRESS:" << std::endl;
	printAddress(ipAddress);
	std::cout << std::endl << "SUBNET MASK:" << std::endl;
	printAddress(subnetMask);
	std::cout << std::endl << "NETWORK:" << std::endl;
	printAddress(networkAddress);
	std::cout << std::endl << "FIRST HOST:" << std::endl;
	printAddress(firstHostAddress);
	std::cout << std::endl << "LAST HOST:" << std::endl;
	printAddress(lastHostAddress);
	std::cout << std::endl << "BROADCAST:" << std::endl;
	printAddress(broadcastAddress);
	std::cout << std::endl;
}
