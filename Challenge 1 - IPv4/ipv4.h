#ifndef IPV4_H
#define IPV4_H
#include <array>
#include <iostream>
#include <cstdint>
#include <string>
#include <sstream>
#include <regex>

std::array<uint8_t, 4> computeNetwork(const std::array<uint8_t, 4>& ipAddress, const std::array<uint8_t, 4>& subnetMask);
std::array<uint8_t, 4> computeBroadcast(const std::array<uint8_t, 4>& subnetMask, const std::array<uint8_t, 4>& networkAddress);
std::array<uint8_t, 4> computefirstHostAddress(const std::array<uint8_t, 4>& networkAddress, const std::array<uint8_t, 4>& subnetMask);
std::array<uint8_t, 4> computelastHostAddress(const std::array<uint8_t, 4>& broadcastAddress, const std::array<uint8_t, 4>& subnetMask);
bool isValidIpAddress(const std::string ipAddress);
bool isValidSubnetMask(const std::string subnetMask);
std::array<uint8_t, 4> readIpAddress();
std::array<uint8_t, 4> readSubnetMask();
std::array<uint8_t, 4> fillUpAddressArray(std::string text);
void computeAddresses();
void printAddress(const std::array<uint8_t, 4>& address);
void printAllAddresses(const std::array<uint8_t, 4>& ipAddress, const std::array<uint8_t, 4>& subnetMask,
					   const std::array<uint8_t, 4>& networkAddress, const std::array<uint8_t, 4>& broadcastAddress,
					   const std::array<uint8_t, 4>& firstHostAddress, const std::array<uint8_t, 4>& lastHostAddress);

#endif