#include "pch.h"
#include "../Challenge 1 - IPv4/ipv4.cpp"

TEST(ipTest, validIpAddress)
{
	EXPECT_TRUE(isValidIpAddress("0.0.0.0"));
	EXPECT_TRUE(isValidIpAddress("255.255.255.255"));
	EXPECT_TRUE(isValidIpAddress("255.0.0.255"));
	EXPECT_TRUE(isValidIpAddress("12.23.34.45"));
}

TEST(ipTest, notValidIpAddress)
{
	EXPECT_FALSE(isValidIpAddress(""));
	EXPECT_FALSE(isValidIpAddress("-1.0.0.0"));
	EXPECT_FALSE(isValidIpAddress("some_text"));
	EXPECT_FALSE(isValidIpAddress("256.255.255.255"));
	EXPECT_FALSE(isValidIpAddress("123.123.123.123.123"));
	EXPECT_FALSE(isValidIpAddress("x.y.z.0"));
}

TEST(maskTest, validSubnetMask)
{
	EXPECT_TRUE(isValidSubnetMask("255.255.255.255"));
	EXPECT_TRUE(isValidSubnetMask("0.0.0.0"));
	EXPECT_TRUE(isValidSubnetMask("255.255.240.0"));
	EXPECT_TRUE(isValidSubnetMask("192.0.0.0"));
}

TEST(maskTest, notValidSubnetMask)
{
	EXPECT_FALSE(isValidSubnetMask("255.240.192.0"));
	EXPECT_FALSE(isValidSubnetMask("255.0.0.255"));
	EXPECT_FALSE(isValidSubnetMask("12.23.34.45"));
	EXPECT_FALSE(isValidIpAddress(""));
	EXPECT_FALSE(isValidIpAddress("-1.0.0.0"));
	EXPECT_FALSE(isValidIpAddress("some_text"));
	EXPECT_FALSE(isValidIpAddress("256.255.255.255"));
	EXPECT_FALSE(isValidIpAddress("123.123.123.123.123"));
	EXPECT_FALSE(isValidIpAddress("x.y.z.0"));
}

TEST(computeNetworkTest)
{
	ASSERT_THAT(computeNetwork({ {0, 0, 0, 0} }, { {0, 0, 0, 0} }), ::testing::ElementsAre(0, 0, 0, 0));
	ASSERT_THAT(computeNetwork({ {0, 0, 0, 0} }, { {255, 255, 255, 255} }), ::testing::ElementsAre(0, 0, 0, 0));
	ASSERT_THAT(computeNetwork({ {255, 255, 255, 255} }, { {0, 0, 0, 0} }), ::testing::ElementsAre(0, 0, 0, 0));
	ASSERT_THAT(computeNetwork({ {255, 255, 255, 255} }, { {255, 255, 255, 255} }), ::testing::ElementsAre(255, 255, 255, 255));
	ASSERT_THAT(computeNetwork({ {192, 168, 0, 1} }, { {255, 240, 0, 0} }), ::testing::ElementsAre(192, 160, 0, 0));
}